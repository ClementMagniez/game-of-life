/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:38:53 CET 2019
*
*/
public interface Observable {
	void attacheObservateur(Observateur o);
	void detacheObservateur(Observateur o);
	void notifieObservateurs();

	
}