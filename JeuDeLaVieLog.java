/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 11:19:49 CET 2019
*
*/
public class JeuDeLaVieLog implements Observateur {
	private JeuDeLaVie jeu;
	private int compteurGen;
	
	
	public JeuDeLaVieLog(JeuDeLaVie jeu) {
		this.compteurGen=0;
		this.jeu=jeu;
	}
	
	public void actualise() {
		int compteurVie=0;
		
		for(int i=0; i<this.jeu.getXMax();i++) {
			for(int j=0; j<this.jeu.getYMax();j++) {
				if(this.jeu.getCell(i,j).estVivante())
					compteurVie+=1;	
			}
		}
		System.out.println("Génération "+ ++this.compteurGen+" : "
							+compteurVie+" cellules vivantes");
	
	}
}