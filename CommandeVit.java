/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:02:54 CET 2019
*
*/
public class CommandeVit extends Commande {
	public CommandeVit(Cellule c) {
		this.cell=c;
	}
	public void executer() {
		this.cell.vit();
	}
}