/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:38:31 CET 2019
*
*/
public interface Observateur {
	void actualise()	;
}