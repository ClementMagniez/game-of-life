/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:35:28 CET 2019
*
*/

import javax.swing.JPanel;
import java.awt.Graphics;

public class JeuDeLaVieUI extends JPanel implements Observateur {
	private JeuDeLaVie jeu;

	public JeuDeLaVieUI(JeuDeLaVie jeu) {
		this.jeu=jeu;
	}
	
	public void actualise() {
		repaint();
	}
	public void paint(Graphics g) {
		int size=Main.SIZE_CELL;

		
		super.paint(g);
		for(int i=0; i<this.jeu.getXMax();i++) {
			for(int j=0; j<this.jeu.getYMax();j++) {
				if(this.jeu.getCell(i,j).estVivante())
					g.fillOval(i*size,j*size,size,size);
			}
		}
	}
}