/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:22:04 CET 2019
*
*/
public class VisiteurClassique extends Visiteur {

	public VisiteurClassique(JeuDeLaVie jeu) {
		super(jeu);
	}

	public void visiteCelluleVivante(Cellule cell) {
		int nbVoisines=cell.nombreVoisinesVivantes(this.jeu);
		if(nbVoisines>3 || nbVoisines < 2)
			this.jeu.ajouteCommande(new CommandeMeurt(cell));
	}
	
	public void visiteCelluleMorte(Cellule cell) {	
		if(cell.nombreVoisinesVivantes(this.jeu)==3)
			this.jeu.ajouteCommande(new CommandeVit(cell));
	}
}