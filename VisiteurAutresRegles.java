/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Apr 03 14:05:27 CEST 2019
*
*/
public class VisiteurAutresRegles extends Visiteur {

	public VisiteurAutresRegles(JeuDeLaVie jeu) {
		super(jeu);
	}

	public void visiteCelluleVivante(Cellule cell) {
		int nbVoisines=cell.nombreVoisinesVivantes(this.jeu);
		// Note : respecte strictement le cahier des charges 
		// ("plus de 8 voisinesvivantes") mais illogique :
		// nbVoisines>8 est impossible
		if(nbVoisines < 3 || nbVoisines==5 || nbVoisines>8)
			this.jeu.ajouteCommande(new CommandeMeurt(cell));
	}
	
	public void visiteCelluleMorte(Cellule cell) {	
		int nbVoisines=cell.nombreVoisinesVivantes(this.jeu);
		if(nbVoisines==3 || nbVoisines>=6)
			this.jeu.ajouteCommande(new CommandeVit(cell));
	}
}