/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:06:07 CET 2019
*
*/
public interface CelluleEtat {
	CelluleEtat vit();
	CelluleEtat meurt();
	boolean estVivante();
	void accepte(Visiteur v, Cellule cell);
}