/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:47:15 CET 2019
*
*/
import javax.swing.JFrame;
import java.awt.Insets;

public class Main {
	
	// Taille de la matrice de jeu SIZE_BOARD*SIZE_BOARD
	public static int SIZE_BOARD=50;

	// Nombre de pixels sur lesquels s'affiche une cellule dans la GUI
	public static int SIZE_CELL=15;
	
	public static void main(String[] args) {
		JeuDeLaVie game=new JeuDeLaVie(SIZE_BOARD,SIZE_BOARD);
		JeuDeLaVieUI gui=new JeuDeLaVieUI(game);
		Observateur log=new JeuDeLaVieLog(game);
		game.attacheObservateur(gui);
		game.attacheObservateur(log);

		JeuDeLaVieHUD window=new JeuDeLaVieHUD(gui,game,
																					 SIZE_BOARD*SIZE_CELL);
	}
}
