/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:06:19 CET 2019
*
*/
public class CelluleEtatMort implements CelluleEtat {

	private static CelluleEtatMort singleton=null;
	
	private CelluleEtatMort() {}

	public static CelluleEtatMort getInstance() {
		if(singleton==null)
			singleton=new CelluleEtatMort();
		return singleton;	
	}
	
	public CelluleEtat meurt() {
		return this;
	}

	public CelluleEtat vit() {
		return CelluleEtatVivant.getInstance();
	}
	
	public boolean estVivante() {
		return false;
	}
	public void accepte(Visiteur v, Cellule cell) {
		v.visiteCelluleMorte(cell);
	}

}