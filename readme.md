Implémentation du jeu de la vie de Conway en Java, interface via Swing.

# Exécuter

`java Main` dans le dossier root

## Manipuler
Par défaut, le jeu avance d'une génération par seconde ; il est possible de le mettre à pause, ce qui débloque le bouton Avancer d'une génération qui rend l'évolution manuelle. La vitesse d'évolution automatique peut changer via le slider ; enfin, le menu déroulant permet de sélectionner l'ensemble de règles classique (par défaut) déterminant la vie ou la mort des cellules, ou l'ensemble alternatif suggéré dans le cahier des charges présent dans ce dossier.

# Contributeurs

Clément Magniez
