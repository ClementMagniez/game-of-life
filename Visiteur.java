/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:12:58 CET 2019
*
*/
abstract public class Visiteur {
	protected JeuDeLaVie jeu;

	public Visiteur(JeuDeLaVie jeu) {
		this.jeu=jeu;
	}

	abstract public void visiteCelluleVivante(Cellule cell);
	abstract public void visiteCelluleMorte(Cellule cell);
}