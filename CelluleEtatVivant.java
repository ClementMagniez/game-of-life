/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:06:14 CET 2019
*
*/
public class CelluleEtatVivant implements CelluleEtat {

	private static CelluleEtatVivant singleton=null;
	
	private CelluleEtatVivant() {}

	public static CelluleEtatVivant getInstance() {
		if(singleton==null)
			singleton=new CelluleEtatVivant();
		return singleton;	
	}

	public CelluleEtat vit() {
		return this;
	}

	public CelluleEtat meurt() {
		return CelluleEtatMort.getInstance();
	}

	public boolean estVivante() {
		return true;
	}
	public void accepte(Visiteur v, Cellule cell) {
		v.visiteCelluleVivante(cell);
	}
}