/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:06:02 CET 2019
*
*/
public class Cellule {
	private CelluleEtat etat;
	private int x,y;

	public Cellule(int x, int y, CelluleEtat etat) {
		this.etat=etat;
		this.x=x;
		this.y=y;
	}
	public void vit() {
		this.etat=this.etat.vit();
	}
	public void meurt() {
		this.etat=this.etat.meurt();
	}
	public boolean estVivante() {
		return this.etat.estVivante();
	}

	public void accepte(Visiteur v) {
		this.etat.accepte(v, this);
	}

	
	public int nombreVoisinesVivantes(JeuDeLaVie jeu) {
		int res=0;

		for(int i=x-1;i<=x+1;i++) {
			for(int j=y-1;j<=y+1;j++) {
				if(i>-1 && i<jeu.getXMax() && j>-1 && j<jeu.getYMax()
				   && (i!=this.x || j!=this.y)
				   && jeu.getCell(i,j).estVivante())
					++res;
			}
		}
		return res;
	}

	public String toString() {
		return "["+this.x+","+this.y+"]";
	}
}