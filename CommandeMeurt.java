/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 10:02:57 CET 2019
*
*/
public class CommandeMeurt extends Commande {
	public CommandeMeurt(Cellule c) {
		this.cell=c;
	}
	public void executer() {
		this.cell.meurt();
	}
}