/**
* @author LeNomDeLEtudiant
* @version 0.1 : Date : Wed Mar 27 09:11:52 CET 2019
*
*/

import java.util.List;
import java.util.LinkedList;

public class JeuDeLaVie implements Observable {
	private Cellule[][] grille;
	private int xMax, yMax;
	private List<Observateur> observateurs;
	private List<Commande> commandes;

	public JeuDeLaVie(int x, int y) {
		this.grille=new Cellule[x][y];
		this.observateurs=new LinkedList<Observateur>();
		this.commandes=new LinkedList<Commande>();
		this.xMax=x;
		this.yMax=y;
		this.initialiseGrille();
	}

	public Cellule getCell(int x, int y) {
		return this.grille[x][y];
	}
	
	public int getYMax() { return this.yMax; }
	public int getXMax() { return this.xMax; }

	public void calculerGenSuivante(Visiteur v) {
		for(Cellule[] ligne : this.grille) {
			for(Cellule cell : ligne) {
				cell.accepte(v);
			}
		}
		this.executeCommandes();
		this.notifieObservateurs();
	}


	public void attacheObservateur(Observateur o) {
		this.observateurs.add(o);
	}
	public void detacheObservateur(Observateur o) {
		this.observateurs.remove(o);
	}
	public void notifieObservateurs() {
		for(Observateur o : this.observateurs) {
			o.actualise();
		}
	}

	public void ajouteCommande(Commande cmd) {
		this.commandes.add(cmd);
	}
	public void executeCommandes() {
		for(Commande cmd : this.commandes) {
			cmd.executer();
		}
		this.commandes.clear();
	}
	
	private void initialiseGrille() {
		for(int i=0;i<this.xMax ; i++) {
			for(int j=0;j<this.yMax;j++) {

				CelluleEtat etat=Math.random()>0.5 ? 
									CelluleEtatMort.getInstance() : 
									CelluleEtatVivant.getInstance(); 
				
				grille[i][j]=new Cellule(i,j,etat);
			}
		}
	}
}