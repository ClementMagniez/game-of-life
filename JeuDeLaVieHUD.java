import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import java.awt.Color; 
import java.awt.Dimension; 
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class JeuDeLaVieHUD {
	private int timing=1000;
	private boolean paused=false;
	private Visiteur visiteur;	
	private JFrame frame;
	private JButton btnPause;
	private JButton btnIncrement;

	public JeuDeLaVieHUD(JeuDeLaVieUI gamePane, JeuDeLaVie game,
											 int taillePane) {
		gamePane.setMinimumSize(new Dimension(taillePane,taillePane));	

		gamePane.setBackground(Color.white);

		this.frame=new JFrame("jeu de la vie");
		this.frame.setLocationRelativeTo(null);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setSize(taillePane+300,
											 taillePane+40); // TODO taille déco

		HashMap<String, Visiteur> mapReglesVisiteur=
																	new HashMap<String, Visiteur>();
																	
		mapReglesVisiteur.put("Classique",new VisiteurClassique(game));
		mapReglesVisiteur.put("Alt",new VisiteurAutresRegles(game));
		this.visiteur=mapReglesVisiteur.get("Classique");

	
		JPanel mainPane=new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.X_AXIS));
	
		// Container des différentes options

		JPanel optionsPane=new JPanel();
		optionsPane.setMaximumSize(new Dimension(200, taillePane));
		optionsPane.setLayout(new BoxLayout(optionsPane,
																				BoxLayout.Y_AXIS));

		// Bouton "pause"
		btnPause=new JButton("Pause");
		btnPause.addActionListener(new Player());
 
 

		optionsPane.add(btnPause);
 		// Bouton "sauter une gen"
		btnIncrement=new JButton("<html>Avancer d'une<br>"+
																			"génération</html>");
		btnIncrement.setEnabled(false);
		btnIncrement.addActionListener(
																	new GenIncrementer(game,
																					this.visiteur));

		optionsPane.add(btnIncrement);
		// Slider "modifier la vitesse d'exécution"
		JLabel labelSlider=new JLabel("Changement de la vitesse");
		
		JSlider timingSlider=new JSlider(50,1000,50);
/*		timingSlider.setMajorTickSpacing(50);
		timingSlider.setPaintLabels(true);
		timingSlider.setLabelTable(
														timingSlider.createStandardLabels(50));
*/
		timingSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				timing=1050-timingSlider.getValue();
			}
		});

		optionsPane.add(labelSlider);
		optionsPane.add(timingSlider);
		
		// Menu "changer les règles de jeu"
		JLabel labelRegles=new JLabel("Changement des règles");
		
		JComboBox listeRegles=new JComboBox(new String[]{"Classique",
																										 "Alt"});
		listeRegles.setPreferredSize(new Dimension(200,60));
		listeRegles.setMaximumSize( listeRegles.getPreferredSize() );
		listeRegles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String key=(String)listeRegles.getSelectedItem();	
				visiteur=mapReglesVisiteur.get(key);
			}
		});


		optionsPane.add(labelRegles);
		optionsPane.add(listeRegles);
		
		
		// Affichage
		
		mainPane.add(gamePane);
		mainPane.add(optionsPane);

		this.frame.setContentPane(mainPane);
		this.frame.setVisible(true);

		this.play(game);

	}


	private void play(JeuDeLaVie game) {
		while(true) {
			System.out.println("Timing : "+this.timing);
			
			if(this.paused) {
				try { Thread.sleep(500); } // TODO solution plus élégante
				catch(InterruptedException e) { e.printStackTrace();}
				finally { continue; }
			}
			
			game.calculerGenSuivante(this.visiteur);
			try { Thread.sleep(this.timing); }
			catch(InterruptedException e) { e.printStackTrace();}
		}
	}
	

	private class Player implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			paused=!paused;
			btnPause.setText(paused ? "Lancer" : "Pause");
			btnIncrement.setEnabled(paused);
		}	
	}
	private class GenIncrementer implements ActionListener {
		private JeuDeLaVie game;
		private Visiteur visiteur;
		private GenIncrementer(JeuDeLaVie game, Visiteur visiteur) {
			this.game=game;
			this.visiteur=visiteur;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(paused) {
				this.game.calculerGenSuivante(this.visiteur);
			}
		}	
	}
	
}
